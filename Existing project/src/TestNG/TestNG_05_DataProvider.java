package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class TestNG_05_DataProvider {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
	}

	@DataProvider(name = "Authentication")
	public static Object[][] Authentication(Method method) {
		Object[][] result = null;
		if (method.getName().equals("TC01")) {
			result = new Object[][] { { "mngr109856", "qArejUn" } };
		} else if (method.getName().equals("TC02")) {
			result = new Object[][] { { "mngr109856", "qArejUn" } };
		}
		return result;
	}

	@Test(dataProvider = "Authentication")
	public void TC01(String username, String password) {
		driver.get("http://demo.guru99.com/v4/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(username);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
	}

	@Test(dataProvider = "Authentication")
	public void TC02(String username, String password) {
		driver.get("http://demo.guru99.com/v4/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(username);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
