package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class TestNG_02_Group {
	@Test(groups ="customer", description = "Create new customer")
	public void TC01_CreateCustomer() {
		System.out.println("Test case 01");
	}

	@Test(groups="customer", description = "Edit customer")
	public void TC02_EditCustomer() {
		System.out.println("Test case 02");
	}

	@Test(groups="customer", description = "Delete customer")
	public void TC03_DeleteCustomer() {
		System.out.println("Test case 03");
	}

	@Test(groups="account", description = "Create Account")
	public void TC04_CreateAccount() {
		System.out.println("Test case 04");
	}
	@Test(groups="account", description = "Edit Account")
	public void TC05_EditAccount() {
		System.out.println("Test case 05");
	}
	@Test(groups="account", description = "Delete Account")
	public void TC06_DeleteAccount() {
		System.out.println("Test case 06");
	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

}
