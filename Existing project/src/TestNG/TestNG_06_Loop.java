package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class TestNG_06_Loop {
	WebDriver driver;
 
  @BeforeClass
  public void beforeClass() {
	  driver= new FirefoxDriver();
  }
  @Parameters({"username","password"})
  @Test(invocationCount = 3 )
	public void TC01_CreateCustomer(String username, String password) {
		driver.get("http://demo.guru99.com/v4/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(username);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		
		String text = driver.findElement(By.xpath("//marquee[@class='heading3']")).getText();
		Assert.assertEquals("Welcome To Manager's Page of Guru99 Bank",text);		
	}
  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

}
