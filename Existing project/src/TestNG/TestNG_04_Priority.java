package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class TestNG_04_Priority {
	@Test(priority = 3, description = "Create new customer")
	public void TC01_CreateCustomer() {
		System.out.println("Test case 01");
	}

	@Test(description = "Edit customer")
	public void TC02_EditCustomer() {
		System.out.println("Test case 02");
	}

	@Test(enabled = true, description = "Delete customer")
	public void TC03_DeleteCustomer() {
		System.out.println("Test case 03");
	}

	@Test(enabled = false, description = "Create Account")
	public void TC04_CreateAccount() {
		System.out.println("Test case 04");
	}
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}

//Priority: follow alphabet: TC01-TC02-TC03 but priority of TC01= 3 --> TC02-TC03-TC01
//enabled = false--> skip test case 