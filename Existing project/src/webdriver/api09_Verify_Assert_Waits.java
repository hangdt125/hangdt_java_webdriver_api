package webdriver;

import org.testng.annotations.Test;

import com.google.common.base.Function;

import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class api09_Verify_Assert_Waits {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {

	}

	@Test
	public void TC01_ImpliciWait() {
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		// step 2
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// step 3
		driver.findElement(By.xpath("//button[contains(text(),'Start')]")).click();
		// step 4
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='finish']//h4")));
		// step 5
		String text = driver.findElement(By.xpath("//div[@id='finish']//h4")).getText();
		Assert.assertEquals(text, "Hello World!");
		driver.quit();
	}

	@Test
	public void TC02_ExpliciWait() {
		driver = new FirefoxDriver();
		// step 1
		driver.get(
				"http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// step 2
		WebDriverWait wait = new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='calendarContainer']")));
		// step 3
		WebElement textonSelectedDate = driver
				.findElement(By.xpath("//div[@id='ctl00_ContentPlaceholder1_ctl00_ContentPlaceholder1_Label1Panel']"));
		Assert.assertEquals("No Selected Dates to display.", textonSelectedDate.getText().trim());
		// step 4
		WebElement selectedate = driver.findElement(By.xpath("//td[a[contains(text(),'20')]]"));
		selectedate.click();
		// step 5
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='raDiv']")));
		// step 6
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//td[@class='rcSelected']//a[contains(text(),'20')]")));
		// step 7
		Assert.assertEquals("Wednesday, December 20, 2017", textonSelectedDate.getText().trim());

		driver.quit();
	}

	@Test
	public void TC03_FluentWait(){
		driver = new FirefoxDriver();
		// step 1
		driver.get("https://stuntcoders.com/snippets/javascript-countdown/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// step 2
		WebElement countdown = driver.findElement(By.xpath("//div[@id='javascript_countdown_time']"));
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOf(countdown));
		// step 3
		  new FluentWait<WebElement>(countdown).withTimeout(10,TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
		  .until(new Function<WebElement, Boolean>() {
		   public Boolean apply(WebElement element) {
		    boolean flag = element.getText().endsWith("02");
		    return flag;
		   }
		  });
		  
		driver.quit();
	}
	@Test
	public void TC04_FluentWait(){
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://toolsqa.wpengine.com/automation-practice-switch-windows/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement countdown_02 = driver.findElement(By.xpath("//*[@id='clock']"));
		  new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(countdown_02));
		  // check changeColor
		  new FluentWait<WebElement>(countdown_02).withTimeout(40, TimeUnit.SECONDS)
		  .pollingEvery(5, TimeUnit.SECONDS) .ignoring(NoSuchElementException.class)
		  .until(new Function<WebElement, Boolean>() {
		   public Boolean apply(WebElement changeColor) {
		    changeColor = driver.findElement(By.xpath("//*[@style='color: red;']"));
		    return changeColor.isDisplayed();
		   }
		  });
		  // text Buzz buzz display
		  new FluentWait<WebElement>(countdown_02).withTimeout(40,TimeUnit.SECONDS).pollingEvery(10,TimeUnit.SECONDS)
		  .until(new Function<WebElement, Boolean>() {
		   public Boolean apply(WebElement element) {
		    boolean flag = element.getText().contains("Buzz Buzz");
		    return flag;
		   }
		  });
		  
		driver.quit();
	}

	@AfterClass
	public void afterClass() {
	}

}
