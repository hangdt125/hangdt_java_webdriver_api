package webdriver;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class api_05_Button_Radio_Checkbox_Alert {
	WebDriver driver;
	WebElement element;

	@BeforeClass
	public void beforeClass() {
	}

	@Test
	public void TC01_Button() throws Exception {
		driver = new FirefoxDriver();

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.findElement(By.xpath(".//*[@id='button-enabled']")).click();
		Assert.assertEquals("http://daominhdam.890m.com/#", driver.getCurrentUrl());
		driver.navigate().back();
		Thread.sleep(500);
		WebElement enable = driver.findElement(By.xpath(".//*[@id='button-enabled']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", enable);

		Assert.assertEquals("http://daominhdam.890m.com/#", driver.getCurrentUrl());

		driver.quit();

	}

	@Test
	public void TC03_() {
		driver = new FirefoxDriver();

		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement checkbox = driver.findElement(By.xpath("//label[contains(text(),'2.0 Petrol, 147kW')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", checkbox);
		if (!checkbox.isSelected()) {
			checkbox.click();
		}

		driver.quit();
	}

	@Test
	public void TC02_Checkbox() throws Exception {
		driver = new FirefoxDriver();

		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement checkbox = driver.findElement(By.xpath("//*[@id='example']//li[label[contains(text(),'Dual-zone air conditioning')]]/input"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", checkbox);
		Assert.assertTrue(checkbox.isSelected());

		if (checkbox.isSelected()) {
			checkbox.click();
			Assert.assertFalse(checkbox.isSelected());
		}

		driver.quit();

	}

	@Test
	public void TC04_Alert(){
		driver = new FirefoxDriver();

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		WebElement button = driver.findElement(By.xpath("//button[contains(text(),'Click for JS Alert')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", button); //click on button to display alert
		
		Alert alert = driver.switchTo().alert();
		String textOnAlert = alert.getText();
		Assert.assertEquals(textOnAlert,"I am a JS Alert"); //verify text on alert
		
		alert.accept(); // accept alert
		
		String message=driver.findElement(By.xpath(".//*[@id='result']")).getText();
		Assert.assertEquals(message,"You clicked an alert successfully"); //compare text after close 
		  
		driver.quit();
	}

	@Test
	public void TC05_Confirm() {
		driver = new FirefoxDriver();

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		WebElement button = driver.findElement(By.xpath("//button[contains(text(), 'Click for JS Confirm')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", button); //click on button to display alert
		
		
		Alert alert = driver.switchTo().alert();
		String textOnAlert = alert.getText();
		Assert.assertEquals(textOnAlert,"I am a JS Confirm"); //verify text on alert
		
		alert.dismiss(); //cancel alert
		
		String message=driver.findElement(By.xpath(".//*[@id='result']")).getText();
		Assert.assertEquals(message,"You clicked: Cancel"); //compare text after close 
		  
		driver.quit();
		
		

	}

	@Test
	public void TC06_Promt() {
		driver = new FirefoxDriver();

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement button = driver.findElement(By.xpath("//button[contains(text(),'Click for JS Prompt')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", button); 

		Alert alert = driver.switchTo().alert();
		String textOnAlert = alert.getText();
		Assert.assertEquals(textOnAlert, "I am a JS prompt"); 
		
		String text= "dao minh dam";

		alert.sendKeys(text); // enter text on alert
		
		alert.accept(); // accept alert

		String message = driver.findElement(By.xpath(".//*[@id='result']")).getText();
		Assert.assertEquals(message, "You entered: "+ text ); 
		
		driver.quit();

	}

	@AfterClass
	public void afterClass() {
	}

}
