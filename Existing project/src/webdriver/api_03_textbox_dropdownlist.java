package webdriver;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class api_03_textbox_dropdownlist {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {

	}

	@Test
	public void TC01_Dropdownlist() throws Exception {
		driver = new FirefoxDriver();

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// khởi tạo dropdown list
		Select dropdown = new Select(driver.findElement(By.xpath(".//*[@id='job1']")));
		// check dropdown list don't support multiple select

		Assert.assertFalse(dropdown.isMultiple());
		// Select option ="Automation Tester" use select visible text
		dropdown.selectByVisibleText("Automation Tester");
		Thread.sleep(2000);
		// check selected value is correct
		Assert.assertEquals("Automation Tester", dropdown.getFirstSelectedOption().getText());
		// Select option ="Manual Tester" use select
		dropdown.selectByValue("manual");
		Thread.sleep(2000);
		Assert.assertEquals("Manual Tester", dropdown.getFirstSelectedOption().getText());
		// Select option ="Mobile Tester" use select Index
		dropdown.selectByIndex(3);

		Thread.sleep(2000);
		Assert.assertEquals("Mobile Tester", dropdown.getFirstSelectedOption().getText());
		// check the dropdown list has all values: 5
		Assert.assertEquals(5, dropdown.getOptions().size());

		driver.quit();

	}

	@Test
	public void TC02_TextboxTextArea() {
		driver = new FirefoxDriver();

		driver.get("http://demo.guru99.com/v4");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Login -step 2
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys("mngr101234");

		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("sYgabad");

		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();

		// step 3
		driver.findElement(By.xpath("//a[contains(text(),'New Customer')]")).click();

		// Step 4-Enter data on New Customer form

		driver.findElement(By.xpath("//input[@name='name']")).sendKeys("automation");
		driver.findElement(By.xpath(".//*[@id='dob']")).sendKeys("01-01-1990");
		driver.findElement(By.xpath("//textarea[@name = 'addr']")).sendKeys("15 Pham Hung");
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Ha Noi");
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys("State");
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys("0123456789");
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys("hang" + random() + "@gmail.com");

		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("123456");

		driver.findElement(By.xpath("//input[@value='Submit']")).click();

		// step 5-Get Customer ID on Customer Registered Successfully!!! form

		String text = driver.findElement(By.xpath("//td[contains(text(),'Customer ID')]/following-sibling::td")).getText();

		// Step 6
		driver.findElement(By.xpath("//a[contains(text(),'Edit Customer')]")).click();

		driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys(text);

		driver.findElement(By.xpath("//input[@name='AccSubmit']")).click();

		// Step 7

		String cusname = driver.findElement(By.xpath("//input[@name='name']")).getAttribute("value");
		// compare user name
		Assert.assertEquals(cusname, "automation");

		String address = driver.findElement(By.xpath("//textarea[@name='addr']")).getText();
		// compare address
		Assert.assertEquals(address, "15 Pham Hung");

		driver.quit();
	}

	// Create random data
	public int random() {

		Random rand = new Random();

		int number = rand.nextInt(100000) + 1;

		return number;
	}

	@AfterClass
	public void afterClass() {
	}

}
