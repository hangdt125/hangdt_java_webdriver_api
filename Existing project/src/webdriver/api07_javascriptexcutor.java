package webdriver;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class api07_javascriptexcutor {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {

	}

	@Test
	public void TC01_javascriptexcutor() throws Exception {
		// step 1
		driver = new FirefoxDriver();
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// step 2
		String domain = (String) executeForBrowserElement(driver, "return document.domain");
		Assert.assertEquals(domain, "live.guru99.com");

		// step 3
		String url = (String) executeForBrowserElement(driver, "return document.URL");
		Assert.assertEquals(url, "http://live.guru99.com/");

		// step 4
		WebElement element1 = driver.findElement(By.xpath("//a[contains(text(),'Mobile')]"));
		executeForWebElement(driver, element1);
		Thread.sleep(3000);

		// step 5
		WebElement element2 = driver.findElement(By.xpath(
				"//h2[a[contains(text(),'Sony Xperia')]]//following-sibling::div[@class='actions']//button[@title='Add to Cart']"));
		highlightElement(driver, element2);
		executeForWebElement(driver, element2);
		Thread.sleep(4000);

		// step 6
		String text = (String) executeForBrowserElement(driver, "return document.documentElement.innerText").toString();
		Assert.assertTrue(text.contains("Sony Xperia was added to your shopping cart."));

		// step 7
		WebElement element3 = driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]"));
		executeForWebElement(driver, element3);
		String title = (String) executeForBrowserElement(driver, "return document.title;").toString();
		Assert.assertEquals(title, "Privacy Policy");

		// step 8
		scrollToBottomPage(driver);

		// step 9
		WebElement element4 = driver.findElement(By.xpath("//tr[th[contains(text(),'WISHLIST_CNT')]]"));
		Assert.assertTrue(element4.isDisplayed());

		// step 10
		executeForBrowserElement(driver, "window.location = 'http://demo.guru99.com/v4/'");
		Thread.sleep(3000);
		String domainotherpage = (String) executeForBrowserElement(driver, "return document.domain;").toString();
		Assert.assertEquals(domainotherpage, "demo.guru99.com");

		driver.quit();

	}

	@Test
	public void TC02_DisableAttribute() throws Exception {
		driver = new FirefoxDriver();
		// step 1
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// step 2
		WebElement frame = driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(frame);
		WebElement element = driver.findElement(By.xpath("//input[@name='lname']"));
		removeAttributeInDOM(driver,element);

		// step 3
		element.sendKeys("Automation Testing");
		Thread.sleep(3000);

		// step 4
		WebElement button = driver.findElement(By.xpath("//input[@value='Submit']"));
		highlightElement(driver,button);
		button.click();

		// step 5
		WebElement result = driver.findElement(By.xpath("//div[@class='w3-container w3-large w3-border']"));
		String text = result.getText();
		Assert.assertTrue(text.contains("Automation Testing"));
		
		driver.switchTo().defaultContent();

		driver.quit();
	}

	// common function:
	public static void highlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='20px groove red'", element);
	}

	public Object executeForWebElement(WebDriver driver, WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object removeAttributeInDOM(WebDriver driver, WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('disabled');", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object executeForBrowserElement(WebDriver driver, String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
