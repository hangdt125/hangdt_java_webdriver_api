package webdriver;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class api_05_useractions {

	WebDriver driver;
	WebElement element;

	@BeforeClass
	public void beforeClass() {
		
	}

	@Test
	public void TC01_MoveMouseToElement1() throws Exception {
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver");
		driver = new ChromeDriver();
		//step 1
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//step 2
		WebElement elem = driver.findElement(By.xpath("//a[contains(text(),'Hover over me')]"));
		Actions builder = new Actions(driver);
		builder.moveToElement(elem).perform();
		Thread.sleep(4000);
		//step 3
		WebElement tooltip = driver.findElement(By.xpath("//div[@class='tooltip-inner']"));
		Assert.assertTrue(tooltip.isDisplayed());
		Thread.sleep(4000);
		//step 4
		String text = tooltip.getText();
		Assert.assertEquals(text,"Hooray!");
		Thread.sleep(4000);		
		driver.quit();		
	}

	@Test
	public void TC02_MoveMouseToElement2() throws Exception {
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver");
		driver = new ChromeDriver();
		//step 1
		driver.get("http://www.myntra.com/");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//step 2
		WebElement elem = driver.findElement(By.xpath("//span[@class='myntraweb-sprite desktop-iconUser sprites-user']"));
		Actions builder = new Actions(driver);
		builder.moveToElement(elem).perform();
		Thread.sleep(4000);
		//step 3
		WebElement loginbutton = driver.findElement(By.xpath("//a[contains(text(),'login')]"));
		loginbutton.click();
		Thread.sleep(4000);
		//step 4
		WebElement loginform =   driver.findElement(By.xpath("//div[@class='login-box']"));   
		Assert.assertTrue(loginform.isDisplayed());
		Thread.sleep(4000);
		driver.quit();	

	}

	@Test
	public void TC03_SelectMultipleItem() throws Exception{
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver");
		driver = new ChromeDriver();
		//step 1
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//step 2
		List<WebElement> listItems = driver.findElements(By.xpath(".//*[@id='selectable']/li"));
		Actions builder = new Actions(driver);
		builder.clickAndHold(listItems.get(0)).clickAndHold(listItems.get(3)).click().perform();
		Thread.sleep(4000);
		//step 3
		List<WebElement> selecteditems = driver.findElements(By.xpath("//li[@class='ui-state-default ui-selectee ui-selected']"));
		int number= selecteditems.size();
		Assert.assertEquals(number,4);
		Thread.sleep(4000);
		driver.quit();		

	}
	@Test
	public void TC04_DoubleClick() throws Exception{
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver");
		driver = new ChromeDriver();
		//step 1
		driver.get("http://www.seleniumlearn.com/double-click");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//step 2
		WebElement elem = driver.findElement(By.xpath("//button[contains(text(),'Double-Click Me!')]"));
		Actions builder = new Actions(driver);
		builder.doubleClick(elem).perform();
		Thread.sleep(4000);
		//step 3
		Alert alert = driver.switchTo().alert();
		String textOnAlert = alert.getText();
		Assert.assertEquals(textOnAlert,"The Button was double-clicked.");
		alert.accept();
		Thread.sleep(4000);
		driver.quit();		

	}
	@Test
	public void TC05_RightClick() throws Exception{
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver");
		driver = new ChromeDriver();
		//step 1		
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//step 2
		WebElement elem = driver.findElement(By.xpath("//span[contains(text(),'right click me')]"));
		Actions action = new Actions(driver).contextClick(elem);
		action.build().perform();
		Thread.sleep(4000);
		//step 3
		WebElement hover = driver.findElement(By.xpath("//li[contains(.,'Quit')]"));
		Actions builder = new Actions(driver);
		builder.moveToElement(hover).perform();
		Thread.sleep(4000);
		//step 4
		WebElement visiblecheck = driver.findElement(By.xpath("//li[contains(@class,'context-menu-visible') and contains(.,'Quit')]"));
		Assert.assertTrue(visiblecheck.isDisplayed());
		Thread.sleep(4000);
		//step 5
		hover.click();
		Thread.sleep(4000);
		//step 6
		Alert alert = driver.switchTo().alert();
		alert.accept();
		Thread.sleep(4000);
		
		Assert.assertFalse(visiblecheck.isDisplayed());
		
		driver.quit();		

	}
	@Test
	public void TC06_DragAndDrop1() throws Exception{
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver");
		driver = new ChromeDriver();
		//step 1
		driver.get("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//step 2
		WebElement dragFrom = driver.findElement(By.xpath(".//*[@id='draggable']"));
		WebElement target = driver.findElement(By.xpath(".//*[@id='droptarget']"));
		Actions builder = new Actions(driver);
		Action dragAndDrop = builder.clickAndHold(dragFrom)
		.moveToElement(target).release(target).build();
		dragAndDrop.perform();
		Thread.sleep(4000);
		//step 3
		String text = target.getText();
		Assert.assertEquals(text,"You did great!");
		Thread.sleep(4000);
		driver.quit();		

	}
	@Test
	public void TC07_DragAndDrop2() throws Exception{
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver");
		driver = new ChromeDriver();
		//step 1
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//step 2
		WebElement dragFrom = driver.findElement(By.xpath(".//*[@id='draggable']"));
		WebElement target = driver.findElement(By.xpath(".//*[@id='droppable']"));
		Actions builder = new Actions(driver);
		Action dragAndDrop = builder.clickAndHold(dragFrom)
		.moveToElement(target).release(target).build();
		dragAndDrop.perform();
		Thread.sleep(4000);
		//step 3
		String text = target.getText();
		Assert.assertEquals(text,"Dropped!");
		Thread.sleep(4000);
		driver.quit();		

	}
	

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
