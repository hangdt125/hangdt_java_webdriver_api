package webdriver;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Unit3_Testcase {
	
  WebDriver driver;	

  @BeforeClass
	  public void beforeClass() {
	  //Pre-condition
	  driver = new FirefoxDriver();
	  driver.get("http://live.guru99.com/");
	  driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
	  
  }	
	
  @SuppressWarnings("deprecation")
@Test
  public void TC01_LoginWithEmptyInfo() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();
	  driver.findElement(By.xpath("//input[@id='email']")).sendKeys("");
	  driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("");
	  driver.findElement(By.xpath("//button[@id='send2']")).click();
	  
	  String errorEmail1 = driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText();
	  Assert.assertEquals("This is a required field.",errorEmail1);
	  
	  
	  String errorPass = driver.findElement(By.xpath("//[@id='advice-required-entry-pass']")).getText();
	  Assert.assertEquals("This is a required field.",errorPass);
	  
  }
  
  @SuppressWarnings("deprecation")
@Test
  public void TC02_LoginWithEmailInvalid() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();
	  driver.findElement(By.xpath("//input[@id='email']")).sendKeys("123434234@12312.123123");
	  driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("");
	  driver.findElement(By.xpath("//button[@id='send2']")).click();
	  
	  String errorEmail2 = driver.findElement(By.xpath("//[@id='advice-validate-email-email']")).getText();
	  Assert.assertEquals("Please enter a valid email address. For example johndoe@domain.com.",errorEmail2);
	  
	  
	  String errorPass2 = driver.findElement(By.xpath("//[@id='advice-required-entry-pass']")).getText();
	  Assert.assertEquals("This is a required field.",errorPass2);
	  
  }
  
  @SuppressWarnings("deprecation")
@Test
  public void TC03_LoginWithPassInvalid() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();
	  driver.findElement(By.xpath("//input[@id='email']")).sendKeys("automation@gmail.com");
	  driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("123");
	  driver.findElement(By.xpath("//button[@id='send2']")).click();
	  
	 
	  String errorPass3 = driver.findElement(By.xpath("//[@id='advice-validate-password-pass']")).getText();
	  Assert.assertEquals("Please enter 6 or more characters without leading or trailing spaces.",errorPass3);
  }
  
  

  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

}
