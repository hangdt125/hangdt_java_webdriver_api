package webdriver;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;

public class api08_uploadfile {

	WebDriver driver;
	String path, filename, emailvalue, folder, fullname;

	@BeforeClass
	public void Data() {
		path = "/Users/hang/Downloads/tooltip.png";
		filename = "tooltip.png";
		emailvalue = "hang" + random() + "@gmail.com";
		fullname = "hangdoan";
		folder = "hang" + random();
	}

	@Test
	public void TC01_UploadSendkey() throws Exception {
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://www.helloselenium.com/2015/03/how-to-upload-file-using-sendkeys.html");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// step 2
		WebElement fileInput = driver.findElement(By.xpath("//input[@name='uploadFileInput']"));
		fileInput.sendKeys(path);
		Thread.sleep(3000);
		driver.quit();

	}

	@Test
	public void TC02_WithoutAutoIT() throws Exception {
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		WebElement upload = driver.findElement(By.xpath("//input[@name='files[]']"));

		upload.sendKeys(path);
		Thread.sleep(3000);

		WebElement file = driver.findElement(
				By.xpath("//table[@class='table table-striped']//p[@class='name' and contains(.,'tooltip.png')]"));
		Assert.assertTrue(file.isDisplayed());
		driver.quit();

	}

	@Test
	public void TC04_Upload() throws Exception {
		driver = new FirefoxDriver();
		// step 1
		driver.get("https://encodable.com/uploaddemo");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// step 2
		WebElement browser = driver.findElement(By.xpath("//input[@id='uploadname1']"));
		browser.sendKeys(path);
		Thread.sleep(3000);

		// step 3
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@name='subdir1']")));
		dropdown.selectByValue("/");
		Thread.sleep(2000);

		// step 4
		WebElement foldername = driver.findElement(By.xpath("//input[@id='newsubdir1']"));
		foldername.sendKeys(folder);

		// step 5
		WebElement email = driver.findElement(By.xpath("//input[@id='formfield-email_address']"));
		email.sendKeys(emailvalue);
		WebElement firstname = driver.findElement(By.xpath("//input[@id='formfield-first_name']"));
		firstname.sendKeys(fullname);

		// step 6
		WebElement button = driver.findElement(By.xpath("//input[@id='uploadbutton']"));
		button.click();

		// step 7
		WebElement emaildisplayed = driver
				.findElement(By.xpath("//dd[contains(text(),'Email Address: " + emailvalue + "')]"));
		Assert.assertTrue(emaildisplayed.isDisplayed());
		WebElement namedisplayed = driver
				.findElement(By.xpath("//dd[contains(text(),'First Name: " + fullname + "')]"));
		Assert.assertTrue(namedisplayed.isDisplayed());
		WebElement filedisplayed = driver.findElement(By.xpath("//dt[a[contains(text(),'" + filename + "')]]"));
		Assert.assertTrue(filedisplayed.isDisplayed());

		// step 8
		driver.findElement(By.xpath("//a[contains(text(),'View Uploaded Files')]")).click();

		// step 9
		WebElement folderdisplayed = driver.findElement(By.xpath("//a[contains(text(),'" + folder + "')]"));
		folderdisplayed.click();

		// step 10
		WebElement image = driver.findElement(By.xpath("//a[contains(text(),'" + filename + "')]"));
		Assert.assertTrue(image.isDisplayed());
		driver.quit();
	}

	public int random() {

		Random rand = new Random();

		int number = rand.nextInt(100000) + 1;

		return number;
	}

	@AfterClass
	public void afterClass() {
	}

}
