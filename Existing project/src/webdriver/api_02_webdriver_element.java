package webdriver;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class api_02_webdriver_element {
	WebDriver driver;
	WebElement element;

	@BeforeClass
	public void beforeClass() {

	}

	@Test
	public void TC01_CreateAnAccount() {
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// step 2
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(),'My Account')]")).click();

		// step 3
		driver.findElement(By.xpath("//a[contains(@title,'Create an Account')]")).click();

		// step 4
		driver.findElement(By.xpath(".//*[@id='firstname']")).sendKeys("Doan Thu"); // input
																					// first
																					// name

		driver.findElement(By.xpath(".//*[@id='lastname']")).sendKeys("Hang"); // input
																				// last
																				// name

		driver.findElement(By.xpath(".//*[@id='email_address']")).sendKeys("automation" + emailrandom() + "@gmail.com"); // input
																															// email

		driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("123456"); // input
																					// password

		driver.findElement(By.xpath(".//*[@id='confirmation']")).sendKeys("123456"); // input
																						// confirm

		driver.findElement(By.xpath("//button[@title = 'Register']")).click();// Click
																				// Register

		// step 5
		String succmess = driver.findElement(By.xpath("//li[@class = 'success-msg']//span")).getText(); // get
																										// text
																										// from
																										// website

		Assert.assertEquals("Thank you for registering with Main Website Store.", succmess); // compare
																								// with
																								// expectation
		driver.quit();

	}

	// Create random data for email
	public int emailrandom() {

		Random rand = new Random();

		int number = rand.nextInt(100000) + 1;

		return number;

	}

	@Test
	public void TC02_DisplayElement() throws Exception {
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// step 2 and step 3
		// Mail
		WebElement mail = driver.findElement(By.xpath(".//*[@id='mail']"));
		if (mail.isDisplayed()) {
			mail.sendKeys("Automation Testing");
		}
		Thread.sleep(500);

		// Age

		WebElement age = driver.findElement(By.xpath("//input[@id='under_18']"));
		if (age.isDisplayed()) {
			age.click();
		}
		Thread.sleep(500);

		// Education
		WebElement education = driver.findElement(By.xpath("//textarea[@id='edu']"));
		if (education.isDisplayed()) {
			education.sendKeys("Automation Testing");
		}
		Thread.sleep(500);

		driver.quit();
	}

	@Test
	public void TC03_EnableDisableElement() throws Exception {
		driver = new FirefoxDriver();

		// step 1
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// step2: Check enable element

		// Mail
		WebElement mail = driver.findElement(By.xpath(".//*[@id='mail']"));
		if (mail.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		Thread.sleep(500);

		// Age
		WebElement age = driver.findElement(By.xpath("//input[@id='under_18']"));
		if (age.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		Thread.sleep(500);

		// Education
		WebElement education = driver.findElement(By.xpath("//textarea[@id='edu']"));
		if (education.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		Thread.sleep(500);

		// Job Role 01
		WebElement job1 = driver.findElement(By.xpath(".//*[@id='job1']"));
		if (job1.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		Thread.sleep(500);

		// Interests (Development)
		WebElement interests = driver.findElement(By.xpath("//label[contains(text(),'Development')]"));
		if (interests.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		Thread.sleep(500);

		// Slider 01
		WebElement slicer1 = driver.findElement(By.xpath(".//*[@id='slider-1']"));
		if (slicer1.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		Thread.sleep(500);

		// Button is enabled
		WebElement enablebutton = driver.findElement(By.xpath(".//*[@id='button-enabled']"));
		if (enablebutton.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		Thread.sleep(500);
		// step3: check disable element
		// Password
		WebElement password = driver.findElement(By.xpath(".//*[@id='password']"));
		if (!password.isEnabled()) {
			System.out.println("Element is disabled");
		} else {
			System.out.println("Element is enabled");
		}
		Thread.sleep(500);

		// Age (Radio button is disabled)
		WebElement disableage = driver.findElement(By.xpath("//label[contains(text(),'Radiobutton is disabled')]"));
		if (!disableage.isEnabled()) {
			System.out.println("Element is disabled");
		} else {
			System.out.println("Element is enabled");
		}
		Thread.sleep(500);

		// Biography
		WebElement bio = driver.findElement(By.xpath(".//*[@id='bio']"));
		if (!bio.isEnabled()) {
			System.out.println("Element is disabled");
		} else {
			System.out.println("Element is enabled");
		}
		Thread.sleep(500);

		// Job Role 02
		WebElement job2 = driver.findElement(By.xpath(".//*[@id='job2']"));
		if (!job2.isEnabled()) {
			System.out.println("Element is disabled");
		} else {
			System.out.println("Element is enabled");
		}
		Thread.sleep(500);

		// Interests (Check box is disabled)
		WebElement interest2 = driver.findElement(By.xpath("//label[contains(text(),'Checkbox is disabled')]"));
		if (!interest2.isEnabled()) {
			System.out.println("Element is disabled");
		} else {
			System.out.println("Element is enabled");
		}
		Thread.sleep(500);

		// Slider 02
		WebElement slider2 = driver.findElement(By.xpath(".//*[@id='slider-2']"));
		if (!slider2.isEnabled()) {
			System.out.println("Element is disabled");
		} else {
			System.out.println("Element is enabled");
		}
		Thread.sleep(500);

		// Button is disabled
		WebElement disbalebutton = driver.findElement(By.xpath(".//*[@id='button-disabled']"));
		if (!disbalebutton.isEnabled()) {
			System.out.println("Element is disabled");
		} else {
			System.out.println("Element is enabled");
		}
		Thread.sleep(500);

		driver.quit();

	}

	@Test
	public void TC04_SelectElement() throws InterruptedException {
		driver = new FirefoxDriver();

		// step 1
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		WebElement age = driver.findElement(By.xpath("//input[@id='under_18']"));
		age.click();
		Thread.sleep(500);
		
		WebElement interests = driver.findElement(By.xpath("//label[contains(text(),'Development')]"));
		interests.click();
		Thread.sleep(500);
		
		if (age.isSelected()) {
			age.click();
		}
		if (interests.isSelected()) {
			interests.click();
		}
		driver.quit();

	}

	@AfterClass
	public void afterClass() {

	}

}
