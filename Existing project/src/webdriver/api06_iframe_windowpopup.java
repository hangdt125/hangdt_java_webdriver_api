package webdriver;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class api06_iframe_windowpopup {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
	}

	@Test
	public void TC01_Iframe() throws Exception {
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// step 2
		WebElement textiframe = driver.findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		driver.switchTo().frame(textiframe);
		WebElement textmessage = driver.findElement(By.xpath(".//*[@id='messageText']"));
		String text = textmessage.getText();
		Assert.assertEquals(text, "What are you looking for?");
		driver.switchTo().defaultContent();

		// step 3
		WebElement banneriframe = driver.findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		driver.switchTo().frame(banneriframe);
		List<WebElement> bannerimage = driver.findElements(By.xpath("//img[@class='bannerimage']"));
		int imagenumber = bannerimage.size();
		Assert.assertEquals(imagenumber, 6);
		driver.switchTo().defaultContent();

		// step 4
		WebElement flipbanner = driver.findElement(By.xpath("//div[@class='flipBanner']"));
		Assert.assertTrue(flipbanner.isDisplayed());

		driver.quit();

	}

	@Test
	public void TC02_Window1() {
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// step 2
		String parentID = driver.getWindowHandle();
		driver.findElement(By.xpath("//a[contains(text(),'Click Here')]")).click();
		switchToChildWindow(parentID);

		// step 3
		String title = driver.getTitle();
		Assert.assertEquals(title, "Google");

		// step 4 and step 5
		closeAllWithoutParentWindows(parentID);

		driver.quit();

	}

	@Test
	public void TC03_Window2() {
		driver = new FirefoxDriver();
		// step 1
		driver.get("http://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// step 2: ignore
		// step 3
		String parentID = driver.getWindowHandle();
		driver.findElement(By.xpath("//a[@href='/htdocs/common/agri/index.html']")).click();
		switchToWindowByTitle("HDFC Bank Kisan Dhan Vikas e-Kendra");

		// step 4
		driver.findElement(By.xpath("//a[contains(.,'Account Details')]")).click();
		switchToWindowByTitle("Welcome to HDFC Bank NetBanking");

		// step 5
		WebElement frame = driver.findElement(By.xpath("//frame[@ name='footer']"));
		driver.switchTo().frame(frame);
		driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]")).click();
		driver.switchTo().defaultContent();
		switchToWindowByTitle(
				"HDFC Bank - Leading Bank in India, Banking Services, Private Banking, Personal Loan, Car Loan");

		// step 6
		driver.findElement(By.xpath("//a[@title='Corporate Social Responsibility']"));

		// step 7
		driver.switchTo().defaultContent();

		// step 8
		closeAllWithoutParentWindows(parentID);

		closeAllWithoutParentWindows(parentID);

		driver.quit();

	}

	public void switchToChildWindow(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(parent)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}
	}

	public boolean closeAllWithoutParentWindows(String parentWindow) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			if (!runWindows.equals(parentWindow)) {
				driver.switchTo().window(runWindows);
				driver.close();
			}
		}
		driver.switchTo().window(parentWindow);
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	public void switchToWindowByTitle(String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			driver.switchTo().window(runWindows);
			String currentWin = driver.getTitle();
			if (currentWin.equals(title)) {
				break;
			}
		}
	}

	@AfterClass
	public void afterClass() {
	}

}
